<?php
require_once "header.php";

require_once "bdd.php";
// On verifie si le user est connecté
if (!empty($_SESSION['login']))
{
    if (!empty($_POST['login']))
    {
       $r = updateProfil($_SESSION['login'], $_POST['login'], $_POST['nom'],
            $_POST['prenom'], $_POST['email'], $_POST['expertise']);

        $_SESSION['login'] = $_POST['login'];
        if (!empty($_FILES) && $_FILES['image']['size'] > 5000000) {
            echo "Error , le fichier est trop lourd !";
        } else if (!empty($_FILES['image']['name']) ) {
            $infosfichier = pathinfo($_FILES['image']['name']);
            $extension_upload = $infosfichier['extension'];

            $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png', 'pdf');
            if (in_array($extension_upload, $extensions_autorisees))
            {
                // ok
                $filename = time().'.'.$extension_upload;
                move_uploaded_file($_FILES['image']['tmp_name'], 'upload/' . basename($filename));

              $r =  updateimageProfil( $_POST['login'], $filename);

            } else {
                echo "L'extension du fichier n'est pas autorisée.";
            }
        }

    }

    $user = getUserByLogin($_SESSION['login']);
    ?>
    <form method="POST" action="" enctype="multipart/form-data">
        <label for="login">Login</label>
        <input type="text" value="<?php echo $user['login']; ?>" required name="login" id="login"><br>

        <label for="email">E mail</label>
        <input type="email" value="<?php echo $user['email']; ?>" required name="email" id="email"><br>
        <label for="nom">Nom</label>
        <input type="text" value="<?php echo $user['nom']; ?>" required name="nom" id="nom"><br>
        <label for="prenom">Prénom</label>
        <input type="text" value="<?php echo $user['prenom']; ?>" required name="prenom" id="prenom"><br>
        <label for="expertise">Expertise</label>
        <select required name="expertise" id="expertise">
            <option value="Débutant" <?php if ($user['expertise'] == "Débutant") { echo "selected";} ?> >Débutant</option>
            <option value="Confirmé" <?php if ($user['expertise'] == "Confirmé") { echo "selected";} ?> >Confirmé</option>
            <option value="Expert" <?php if ($user['expertise'] == "Expert") { echo "selected";} ?> >Expert</option>
        </select>
            <br>
        <label for="image">Photo de profil : </label>
        <?php if ($user['image'] != NULL ) { echo "<img src='upload/".$user['image']."' width='50px' > <br>"; }
            else {
                echo "Aucune photo de profil connue<br>";
            }
        ?>
        <input type="file" name="image" id="image"><br>
        <input type="submit" value="Sauvegarder">
    </form>
    </body>
    </html>

<?php
}

?>
