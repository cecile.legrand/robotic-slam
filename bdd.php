<?php
function connectBdd() {
    $dsn = 'mysql:dbname=robotic;host=127.0.0.1';
    $user = 'slam';
    $password = 'slam';

    $dbh = new PDO($dsn, $user, $password);
    return $dbh;
}

function insertFiche($createur, $titre, $description, $tps, $complexite, $image) {
    $bdd = connectBdd();
    $req = $bdd->prepare("INSERT INTO fiche ( createur, titre, description,tps_realisation, complexite, image) 
                        VALUES ( :createur , :titre, :desc, :tps, :complexite, :image ) ");
    $req->execute(array( ":createur" => $createur , ":titre" => $titre , ":desc" => $description, ":tps" => $tps,
        ":complexite" => $complexite , ":image" => $image) );
}

function addUtilisateur($login, $mdp, $prenom,$nom,$email)
{
    $bdd = connectBdd();
    $req = $bdd->prepare("INSERT INTO utilisateur ( login, mdp, nom,prenom, email) 
                        VALUES ( :login , :mdp, :nom, :prenom, :email ) ");
    $req->execute(array( ":login" => $login , ":mdp" => $mdp , ":nom" => $nom, ":prenom" => $prenom, ":email" => $email ) );
    return $req->errorInfo();
}

function  updateProfil($oldlogin, $login, $nom,$prenom, $email, $expertise)
{
    $bdd = connectBdd();
    $req = $bdd->prepare("UPDATE utilisateur SET  login = :login, nom = :nom ,prenom = :prenom
                     , email = :email , expertise = :expertise WHERE login = :oldlogin
                         ");
    $req->execute(array( ":login" => $login , ":oldlogin" => $oldlogin, ":expertise" => $expertise
    , ":nom" => $nom, ":prenom" => $prenom, ":email" => $email ) );
    return $req->errorInfo();
}
function  updateimageProfil( $login, $image)
{
    $bdd = connectBdd();
    $req = $bdd->prepare("UPDATE utilisateur SET  image = :image WHERE login = :login
                         ");
    $req->execute(array( ":login" => $login ,  ":image" => $image ) );
    return $req->errorInfo();
}



function getAllFiches() {
    $bdd = connectBdd();
    $req = $bdd->query("SELECT *  FROM fiche");
    return $req->fetchAll();
}
function getConnexion($login, $mdp) {
    $bdd = connectBdd();
    $req = $bdd->prepare("SELECT *  FROM utilisateur where login = :login and mdp = :mdp");
    $req->execute(array(  ":login" => $login, ":mdp" => $mdp  ));
    return $req->fetch();
}

function getUserByLogin($login) {
    $bdd = connectBdd();
    $req = $bdd->prepare("SELECT *  FROM utilisateur where login = :login");
    $req->execute(array(  ":login" => $login ));
    return $req->fetch();
}

function getFicheByTitre($titre) {
    $bdd = connectBdd();
    $req = $bdd->prepare("SELECT * FROM fiche WHERE titre LIKE :titre");
    $req->execute(array(  ":titre" => '%'.$titre.'%'  ));
    return $req->fetchAll();
}
