<?php
    require_once 'header.php';

    if (empty($_SESSION['login'])) {
        http_response_code(403);
        die('Accès Interdit - Au revoir !');
    }

?>
    <h1>Ajout d'une fiche </h1>
    <form method="POST" action="" enctype="multipart/form-data">
        <label for="titre">Titre</label>
        <input type="text" name="titre" id="titre"> <br>
        <label for="description">Description</label>
        <textarea name="description" id="description"></textarea> <br>
        <label for="tps_realisation">Temps réalisation (min)</label>
        <input type="number" name="tps_realisation" id="tps_realisation"> <br>
        <label for="image">Image</label>
        <input type="file"  name="image" id="image"><br>
        <label for="complexite">Compléxité</label>
        <input type="range" min="0" max="5" name="complexite" id="complexite"><br>
        <input type="submit" value="Ajouter une fiche">
    </form>
</body>

</html>

<?php

  if (isset($_POST['titre'])) {

      require_once 'bdd.php';

      var_dump($_FILES);
      if (!empty($_FILES) && $_FILES['image']['size'] > 5000000) {
          echo "Error , le fichier est trop lourd !";
      } else {
          $infosfichier = pathinfo($_FILES['image']['name']);
          $extension_upload = $infosfichier['extension'];

          $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png', 'pdf');
          if (in_array($extension_upload, $extensions_autorisees))
          {
                // ok
              $filename = time().'.'.$extension_upload;
              move_uploaded_file($_FILES['image']['tmp_name'], 'upload/' . basename($filename));

              echo "L'envoi a bien été effectué !";
              insertFiche(1, $_POST['titre'], $_POST['description'], $_POST['tps_realisation'], $_POST['complexite'], $filename);
              //  header("Location: index.php");

          } else {
              echo "L'extension du fichier n'est pas autorisée.";
          }
      }



  }

?>
